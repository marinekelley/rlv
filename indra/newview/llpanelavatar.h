/**
 * @file llpanelavatar.h
 * @brief Legacy profile panel base class
 *
 * $LicenseInfo:firstyear=2019&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2019, Linden Research, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#ifndef LL_LLPANELAVATAR_H
#define LL_LLPANELAVATAR_H

#include "llcallingcard.h"
#include "llpanel.h"
#include "llavatarpropertiesprocessor.h"
#include "llavatarnamecache.h"
#include "llvoiceclient.h"

class LLComboBox;
class LLLineEditor;
class LLToggleableMenu;

enum EOnlineStatus
{
    ONLINE_STATUS_NO      = 0,
    ONLINE_STATUS_YES     = 1
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Class LLProfileDropTarget
//
// This handy class is a simple way to drop something on another
// view. It handles drop events, always setting itself to the size of
// its parent.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class LLProfileDropTarget : public LLView
{
public:
    struct Params : public LLInitParam::Block<Params, LLView::Params>
    {
        Optional<LLUUID> agent_id;
        Params()
        :   agent_id("agent_id")
        {
            changeDefault(mouse_opaque, false);
            changeDefault(follows.flags, FOLLOWS_ALL);
        }
    };

    LLProfileDropTarget(const Params&);
    ~LLProfileDropTarget() {}

    //
    // LLView functionality
    virtual bool handleDragAndDrop(S32 x, S32 y, MASK mask, bool drop,
                                   EDragAndDropType cargo_type,
                                   void* cargo_data,
                                   EAcceptance* accept,
                                   std::string& tooltip_msg);

    void setAgentID(const LLUUID &agent_id)     { mAgentID = agent_id; }

protected:
    LLUUID mAgentID;
};


/**
* Base class for any Profile View.
*/
class LLPanelProfileTab
    : public LLPanel
{
public:

    /**
     * Sets avatar ID, sets panel as observer of avatar related info replies from server.
     */
    virtual void setAvatarId(const LLUUID& avatar_id);

    /**
     * Returns avatar ID.
     */
    virtual const LLUUID& getAvatarId() const { return mAvatarId; }

    /**
     * Sends update data request to server.
     */
    virtual void updateData() {};

    /**
     * Clears panel data if viewing avatar info for first time and sends update data request.
     */
    virtual void onOpen(const LLSD& key) override;

    /**
     * Clears all data received from server.
     */
    virtual void resetData(){};

    /*virtual*/ ~LLPanelProfileTab();

protected:

    LLPanelProfileTab();

    enum ELoadingState
    {
        PROFILE_INIT,
        PROFILE_LOADING,
        PROFILE_LOADED,
    };


    // mLoading: false: Initial state, can request
    //           true:  Data requested, skip duplicate requests (happens due to LLUI's habit of repeated callbacks)
    // mLoaded:  false: Initial state, show loading indicator
    //           true:  Data recieved, which comes in a single message, hide indicator
    ELoadingState getLoadingState() { return mLoadingState; }
    virtual void setLoaded();
    void setApplyProgress(bool started);

    const bool getSelfProfile() const { return mSelfProfile; }

    bool saveAgentUserInfoCoro(std::string name, LLSD value, std::function<void(bool)> callback = nullptr) const;

public:
    void setIsLoading() { mLoadingState = PROFILE_LOADING; }
    void resetLoading() { mLoadingState = PROFILE_INIT; }

    bool getStarted() { return mLoadingState != PROFILE_INIT; }
    bool getIsLoaded() { return mLoadingState == PROFILE_LOADED; }

    virtual bool hasUnsavedChanges() { return false; }
    virtual void commitUnsavedChanges() {}

private:

    LLUUID  mAvatarId;
    ELoadingState    mLoadingState;
    bool    mSelfProfile;
};

class LLPanelProfilePropertiesProcessorTab
    : public LLPanelProfileTab
    , public LLAvatarPropertiesObserver
{
public:
    LLPanelProfilePropertiesProcessorTab();
    ~LLPanelProfilePropertiesProcessorTab();

    void setAvatarId(const LLUUID& avatar_id) override;

    void updateData() override;

    /**
     * Processes data received from server via LLAvatarPropertiesObserver.
     */
    virtual void processProperties(void* data, EAvatarProcessorType type) override = 0;
};

/**
* Panel for displaying Avatar's first and second life related info.
*/
class LLPanelAvatarProfile
    : public LLPanelProfileTab
    , public LLFriendObserver
    , public LLVoiceClientStatusObserver
{
public:
    LLPanelAvatarProfile();
    /*virtual*/ ~LLPanelAvatarProfile();

    /*virtual*/ void onOpen(const LLSD& key) override;

    /**
     * LLFriendObserver trigger
     */
    virtual void changed(U32 mask) override;

    // Implements LLVoiceClientStatusObserver::onChange() to enable the call
    // button when voice is available
    /*virtual*/ void onChange(EStatusType status, const LLSD& channelInfo, bool proximal) override;

    /*virtual*/ void setAvatarId(const LLUUID& id) override;

    /**
     * Processes data received from server.
     */
    /*virtual*/ void processProperties(void* data, EAvatarProcessorType type);

    /*virtual*/ bool postBuild() override;

    /*virtual*/ void updateData() override;

    /*virtual*/ void resetControls();

    /*virtual*/ void resetData() override;

protected:

    /**
     * Process profile related data received from server.
     */
    virtual void processProfileProperties(const LLAvatarData* avatar_data);

    /**
     * Processes group related data received from server.
     */
//  virtual void processGroupProperties(const LLAvatarGroups* avatar_groups);

    /**
     * Fills common for Avatar profile and My Profile fields.
     */
    virtual void fillCommonData(const LLAvatarData* avatar_data);

    /**
     * Fills partner data.
     */
    virtual void fillPartnerData(const LLAvatarData* avatar_data);

    /**
     * Fills account status.
     */
    virtual void fillAccountStatus(const LLAvatarData* avatar_data);

    /**
     * Opens "Pay Resident" dialog.
     */
    void pay();

    /**
     * opens inventory and IM for sharing items
     */
    void share();

    /**
     * Add/remove resident to/from your block list.
     */
    void toggleBlock();

    void kick();
    void freeze();
    void unfreeze();
    void csr();

    bool enableShowOnMap();
    bool enableBlock();
    bool enableUnblock();
    bool enableGod();

    void onSeeProfileBtnClick();
    void onAddFriendButtonClick();
    void onIMButtonClick();
    void onCallButtonClick();
    void onTeleportButtonClick();
    void onShareButtonClick();
    void onOverflowButtonClicked();

private:
    void onNameCache(const LLUUID& agent_id, const LLAvatarName& av_name);

    typedef std::map< std::string,LLUUID>   group_map_t;
    group_map_t             mGroups;

    void                    openGroupProfile();

    LLToggleableMenu*       mProfileMenu;
};

/**
 * Panel for displaying own first and second life related info.
 */
class LLPanelMyProfile
    : public LLPanelAvatarProfile
{
public:
    LLPanelMyProfile();

    /*virtual*/ bool postBuild();

protected:

    /*virtual*/ void onOpen(const LLSD& key);

    /*virtual*/ void processProfileProperties(const LLAvatarData* avatar_data);

    /*virtual*/ void resetControls();

protected:
    void onStatusMessageChanged();
};

/**
 * Panel for displaying Avatar's notes and modifying friend's rights.
 */
class LLPanelAvatarNotes
    : public LLPanelProfileTab
    , public LLFriendObserver
    , public LLVoiceClientStatusObserver
{
public:
    LLPanelAvatarNotes();
    /*virtual*/ ~LLPanelAvatarNotes();

    virtual void setAvatarId(const LLUUID& id) override;

    /**
     * LLFriendObserver trigger
     */
    virtual void changed(U32 mask) override;

    // Implements LLVoiceClientStatusObserver::onChange() to enable the call
    // button when voice is available
    /*virtual*/ void onChange(EStatusType status, const LLSD& channelInfo, bool proximal) override;

    /*virtual*/ void onOpen(const LLSD& key) override;

    /*virtual*/ bool postBuild() override;

    /*virtual*/ void processProperties(void* data, EAvatarProcessorType type);

    /*virtual*/ void updateData() override;

protected:

    /*virtual*/ void resetControls();

    /*virtual*/ void resetData() override;

    /**
     * Fills rights data for friends.
     */
    void fillRightsData();

    void rightsConfirmationCallback(const LLSD& notification,
            const LLSD& response, S32 rights);
    void confirmModifyRights(bool grant, S32 rights);
    void onCommitRights();
    void onCommitNotes();

    void onAddFriendButtonClick();
    void onIMButtonClick();
    void onCallButtonClick();
    void onTeleportButtonClick();
    void onShareButtonClick();
    void enableCheckboxes(bool enable);
};

/**
 * Panel for displaying Avatar's firstlist profile.
 */
class LLPanelAvatarFirst
    : public LLPanelProfileTab
    , public LLFriendObserver
    , public LLVoiceClientStatusObserver
{
public:
    LLPanelAvatarFirst();
    /*virtual*/ ~LLPanelAvatarFirst();

    virtual void setAvatarId(const LLUUID& id) override;

    /**
     * LLFriendObserver trigger
     */
    virtual void changed(U32 mask) override;

    // Implements LLVoiceClientStatusObserver::onChange() to enable the call
    // button when voice is available
    /*virtual*/ void onChange(EStatusType status, const LLSD& channelInfo, bool proximal) override;

    /*virtual*/ void onOpen(const LLSD& key) override;

    /*virtual*/ bool postBuild() override;

    /*virtual*/ void processProperties(void* data, EAvatarProcessorType type);

    /*virtual*/ void updateData() override;

protected:

    /*virtual*/ void resetControls();

    /*virtual*/ void resetData() override;

};

#endif // LL_LLPANELAVATAR_H
